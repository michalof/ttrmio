#pragma once
#include "AiBase.h"

#include <map>

using namespace std;
using namespace ttrapi;

struct PathMetrics
{
	PathMetrics() {}
	PathMetrics(const vector<Route*>& _routes) { calcRoutes(_routes); }

	void calcRoutes(const vector<Route*>& _routes);
	void addPoints(size_t _points);

	size_t	trains = 0;
	size_t	length = 0;
	size_t	points = 0;
	size_t	hops = 0;
	size_t	turns = 0;
	float	ppt = 0;
};

struct RouteInstructions
{
	TrainCardColor	color = TCC_NONE;
	size_t			extraLocos = 0;
	const RouteDef*	selectedRoute = nullptr;
	Route*			route = nullptr;
	size_t			priority = 0;
	bool			possible = false;

	bool operator < (const RouteInstructions& _o) const {
		if (priority < _o.priority)
			return true;
		return extraLocos < _o.extraLocos;
	}
};

class AiMiO : public AiBase
{
public:
	AiMiO(const AiOptions& _opt);

protected:
	void loadHashLookup();
	float lookupHashTime(const char* _hash);

	virtual void think(Token& _token);
	virtual void thinkFirstRound(Token& _token);

	void thinkBuildLong(Token& _token);
	void thinkBuildTickets(Token& _token);
	void thinkFinish(Token& _token);
	void thinkLastRound(Token& _token);

	void buildRoutes(Token& _token, vector<Route*> _routes);
	bool pickTrainCard(Token& _token, const vector<Route*>& _routes, bool _allowLoco);
	TrainCardColor pickTrain(Token & _token, const vector<RouteInstructions>& _routes, bool _preBuild, bool _allowLoco);

	vector<RouteInstructions> planRouteConstruction(const vector<Route*>& _routes);

	vector<Route*> findPathForTickets(const vector<TicketDef>& _tickets);
	void ticketDecision(vector<TicketDecision>& _tickets);

private:
	function<void(Token&)> m_Phase;
	vector<pair<uint64_t, float> > m_HashLookup;
	vector<TicketDef> m_Tickets;
};

