#include "AiMiO.h"
#include <iostream>
#include <cstdlib>

#include <WinSock2.h>
#include <assert.h>

using namespace std;
using namespace ttrapi;

AiMiO::AiMiO(const AiOptions & _opt) : AiBase(_opt)
{
	m_Phase = bind(&AiMiO::thinkBuildLong, this, placeholders::_1);
//	loadHashLookup();
}

void AiMiO::loadHashLookup()
{
	ifstream idx("hash.idx", ofstream::binary);

	idx.seekg(0, ios_base::end);
	size_t elements = idx.tellg() / sizeof(m_HashLookup[0]);
	idx.seekg(0);

	m_HashLookup.resize(elements);
	idx.read((char*)m_HashLookup.data(), elements * sizeof(m_HashLookup[0]));
}

float AiMiO::lookupHashTime(const char * _hash)
{
	char buffer[17];
	strncpy_s(buffer, _hash, 16);
	buffer[16] = 0;

	uint64_t hash = strtoull(buffer, nullptr, 16);

	auto it = lower_bound(m_HashLookup.begin(), m_HashLookup.end(), make_pair(hash, 0.f));
	if (it != m_HashLookup.end() && it->first == hash)
		return it->second;
	return 0.0f;
}

void hashOfTimestamp(float _time, unsigned char* _hash);
float nextByTimeStep(float _f);

void AiMiO::think(ttrapi::Token & _token)
{
#if 1
	for(const Player& p : state().player)
		if (p.numTrainTokens <= 2)
		{
			thinkLastRound(_token);
			return;
		}

	m_Phase(_token);
#else
	float startTime = lookupHashTime(m_Conn->hash());
	ofstream("times.txt", ofstream::app) << startTime << '\n';

	if (startTime != 0)
	{
		stringstream cmd;
		cmd << m_Conn->hash() << " CoTC\n" << m_Conn->hash() << " CoTC\n";
		cmd << hex;

		for (float t = startTime + 2.633f; t < startTime + 2.733f; t = nextByTimeStep(t))
		{
			unsigned char md5[16];
			hashOfTimestamp(t, md5);

			for (unsigned char n : md5)
				cmd << (size_t)n;
			cmd << " DeTi\n";
		}

		string cmdStr = cmd.str();
		::send(m_Conn->getSocket(), cmdStr.c_str(), cmdStr.length(), 0);

		m_Conn->recv(4);
		m_Conn->recv(4);
	}
	else
	{
		m_Phase(_token);
	}
#endif
}

void AiMiO::thinkFirstRound(Token & _token)
{
	_token.drawDestinationTickets(bind(&AiMiO::ticketDecision, this, placeholders::_1));
}

void AiMiO::thinkBuildLong(ttrapi::Token & _token)
{
	vector<Route*> routes[2];
	size_t mine = 0;
	for (auto& r : m_Map.routes())
	{
		if (r.def->length < 6)
			continue;
		if (r.owner && r.owner->id == player().id)
			mine++;

		if (r.available() && r.def->length == 8)
			routes[0].push_back(&r);
		else if (r.available() && r.def->length == 6)
			routes[1].push_back(&r);
	}

	if (mine > 1 || routes[0].empty() && routes[1].empty())
	{
	//	_token.drawDestinationTickets();
		m_Phase = bind(&AiMiO::thinkBuildTickets, this, placeholders::_1);
		thinkBuildTickets(_token);
	}
	else
	{
		buildRoutes(_token, move(routes[0].empty() ? routes[1] : routes[0]));
	}
}

void AiMiO::thinkBuildTickets(ttrapi::Token & _token)
{
	Map<> map;
	map.updateOwner(state());

	array<int, NUM_ROUTES> routeCost;
	for (size_t i = 0; i < NUM_ROUTES; ++i)
	{
		const Route& r = map.routes()[i];
		if (r.owner && r.owner->id == player().id)
			routeCost[i] = 0;
		else if (!r.available() || r.def->length > player().numTrainTokens)
			routeCost[i] = -1;
		else
			routeCost[i] = r.def->length > 3 ? 1 : 2;
	}
	auto costFunc = [&](const CityLink& _l) { return routeCost[_l.route.def->id - 1]; };

	vector<Route*> routes;

	for (const TicketDef& ticket : m_Tickets)
	{
		vector<CityLink*> path = map.dijkstra(ticket.city[0], ticket.city[1], costFunc);
		for (CityLink* link : path)
		{
			if (!link->route.owner)
			{
				routes.push_back(&m_Map.routes()[link->route.def->id - 1]);
				link->route.owner = &player();
			}
		}
	}

	if (routes.empty())
	{
		m_Phase = bind(&AiMiO::thinkFinish, this, placeholders::_1);
		thinkFinish(_token);
	}
	else
	{
		buildRoutes(_token, routes);
	}
}

void AiMiO::thinkFinish(ttrapi::Token & _token)
{
	vector<Route*> routes;
	int tokens = player().numTrainTokens;

	for (size_t j = 0; j < 2; ++j)
	{
		for (size_t i = 4; i > 0 && routes.empty(); --i)
		{
			if (i > player().numTrainTokens)
				continue;

			for (auto& r : m_Map.routes())
			{
				if (r.available() && r.def->length == i && tokens >= i &&
					(j != 0 || r.def->color == TCC_ALL && !r.def->tunnle && !r.def->locos))
				{
					tokens -= r.def->length;
					routes.push_back(&r);
				}
			}
		}
	}
	buildRoutes(_token, move(routes));
}

void AiMiO::thinkLastRound(Token & _token)
{
	int bestPoints = 0;
	vector<Route*> routes;
	array<bool, NUM_ROUTES> myRoutes;
	transform(state().routeOwner.begin(), state().routeOwner.end(), myRoutes.begin(), [&](uint8_t _id) { return _id == player().id; });

	for (Route& r : m_Map.routes())
	{
		if (r.canBuild(player()) == TCC_NONE)
			continue;

		myRoutes[r.def->id - 1] = true;
		int points = state().points(player(), myRoutes);
		if (state().longestPathPlayer() == player().id)
			points += POINTS_FOR_LONGEST_PATH;

		if (points == bestPoints)
		{
			routes.push_back(&r);
		}
		else if (points > bestPoints)
		{
			routes.clear();
			routes.push_back(&r);
			bestPoints = points;
		}
		myRoutes[r.def->id - 1] = false;
	}

	if (routes.empty())
	{
		_token.drawCoverdTrainCard();
		_token.drawCoverdTrainCard();
	}
	else
	{
		for (Route* r : routes)
		{
			if (!r->def->tunnle)
			{
				_token.buildRoute(*r->def, r->canBuild(player()));
				return;
			}
		}
		for (Route* r : routes)
		{
			_token.buildRoute(*r->def, r->canBuild(player()));
			return;
		}
	}
}

void AiMiO::buildRoutes(ttrapi::Token & _token, vector<Route*> _routes)
{
	vector<RouteInstructions> routes = planRouteConstruction(_routes);

	cout << endl << "-----------------------------------------------------------------------" << endl;
	for (RouteInstructions& r : routes)
		cout << r.route->city[0]->cityName() << "-" << r.route->city[1]->cityName() << " " << r.priority << " " << g_ColorNames[r.color] << endl;


	if (pickTrain(_token, routes, true, false) != TCC_NONE)
	{
		pickTrain(_token, routes, false, false);
		return;
	}
	
	for (const RouteInstructions& r : routes)
	{
		if (r.priority < routes.front().priority)
			break;
		
		if (r.possible)
		{
			assert(r.route->canBuild(player(), r.color));
			_token.buildRoute(*r.selectedRoute, r.color);
			return;
		}
	}

	if(pickTrain(_token, routes, false, true) != TCC_LOCO)
		pickTrain(_token, routes, false, false);
}

TrainCardColor AiMiO::pickTrain(Token & _token, const vector<RouteInstructions>& _routes, bool _preBuild, bool _allowLoco)
{
	uint8_t openSpot[TCC_NONE] = {};
	uint8_t openTrains[TCC_NONE] = {};
	for (uint8_t i = 0; i < state().openTrainCards.size(); ++i)
		if (state().openTrainCards[i] < TCC_NONE)
		{
			openTrains[state().openTrainCards[i]]++;
			openSpot[state().openTrainCards[i]] = i;
		}

	for (const RouteInstructions& r : _routes)
	{
		if (!openTrains[r.color] || r.extraLocos == 0)
			continue;

		if (r.selectedRoute->color == TCC_ALL && _preBuild && player().trainCards[r.color] < 2)
			continue;

		if (_preBuild && r.priority + 1 < _routes.front().priority)
			break;

		_token.drawOpenTrainCard(openSpot[r.color]);
		return r.color;
	}

	if (!_preBuild)
	{
		if (_allowLoco && openTrains[TCC_LOCO] && m_Conn->currentRound() > 6)
		{
			for (const RouteInstructions& r : _routes)
			{
				if (r.selectedRoute->locos > player().trainCards[TCC_LOCO] ||
					r.selectedRoute->tunnle && player().trainCards[TCC_LOCO] == 0)
				{
					_token.drawOpenTrainCard(openSpot[TCC_LOCO]);
					return TCC_LOCO;
				}
			}
		}

		_token.drawCoverdTrainCard();
		return TCC_ALL;
	}
	return TCC_NONE;
}

vector<RouteInstructions> AiMiO::planRouteConstruction(const vector<Route*>& _routes)
{
	vector<RouteInstructions> out(_routes.size());
	for (size_t i = 0; i < _routes.size(); ++i)
	{
		out[i].route = _routes[i];
		out[i].priority = max<size_t>(_routes[i]->def->length, _routes[i]->def->tunnle ? 4 : 0);
		out[i].selectedRoute = _routes[i]->def;
	}
	sort(out.rbegin(), out.rend());

	array<int, TCC_NONE> availableCards = {};
	for (size_t i = 1; i < TCC_NONE; ++i)
		availableCards[i] = player().trainCards[i];
	for (TrainCardColor c : state().openTrainCards)
		if(c != TCC_LOCO && c < TCC_NONE)
			availableCards[c]++;

	// assign colored routes and select twins
	for (RouteInstructions& r : out)
	{
		if (r.selectedRoute->color != TCC_ALL)
		{
			if (r.route->twin && r.route->twin->available() && 
				availableCards[r.route->twin->def->color] > availableCards[r.route->def->color])
			{
				r.selectedRoute = r.route->twin->def;
			}
			r.color = r.selectedRoute->color;
			availableCards[r.color] -= (int)r.route->ticketCost();
		}
	}

	// assigned colors to gray routes
	for (RouteInstructions& r : out)
	{
		if (r.selectedRoute->color == TCC_ALL)
		{
			auto color = max_element(++availableCards.begin(), availableCards.end()) - availableCards.begin();
			r.color = (TrainCardColor)color;
			availableCards[color] -= (int)r.route->ticketCost();
		}
	}

	// update possibility
	for (RouteInstructions& r : out)
	{
		r.extraLocos = r.route->ticketCost() > player().trainCards[r.color] ? r.route->ticketCost() - player().trainCards[r.color] : 0;
		r.possible = r.extraLocos + r.selectedRoute->locos <= player().trainCards[TCC_LOCO];
	}
	return out;
}

vector<Route*> AiMiO::findPathForTickets(const vector<TicketDef>& _tickets)
{
	array<int, NUM_ROUTES> routeCost;
	for (size_t i = 0; i < NUM_ROUTES; ++i)
	{
		const Route& r = m_Map.routes()[i];
		if (r.owner && r.owner->id == player().id)
			routeCost[i] = 0;
		else if (!r.available() || r.def->length > player().numTrainTokens)
			routeCost[i] = -1;
		else
			routeCost[i] = r.def->length > 3 ? 1 : 2;
	//		routeCost[i] = 1;
	//		routeCost[i] = r.def->length;
	}
	auto costFunc = [&](const CityLink& _l) { return routeCost[_l.route.def->id - 1]; };

	vector<Route*> routes;

	for (const TicketDef& ticket : _tickets)
	{
		vector<CityLink*> path = m_Map.dijkstra(ticket.city[0], ticket.city[1], costFunc);
		for (CityLink* link : path)
		{
			if (!link->route.owner)
			{
				size_t idx = link->route.def->id - 1;
				routes.push_back(&m_Map.routes()[idx]);
				routeCost[idx] = 0;
			}
		}
	}
	return routes;
}

void AiMiO::ticketDecision(vector<TicketDecision>& _tickets)
{
	const size_t trainTokenLimit = 45 - 8 - 6 - 4;
	const size_t minTickets = min<size_t>(_tickets.size(), state().player.size() <= 2 ? 2 : 1);

	float bestPpt = 0;
	vector<TicketDecision*> bestTicket;

	vector<size_t> ticketPerm(_tickets.size());
	for (size_t i = 0; i < ticketPerm.size(); ++i)
		ticketPerm[i] = i;

	vector<TicketDef> ticketDef;
	do {
		ticketDef.clear();
		size_t cardPoints = 0;

		for (size_t i = 0; i < minTickets - 1; ++i)
		{
			ticketDef.push_back(_tickets[ticketPerm[i]]);
			cardPoints += ticketDef.back().points;
		}

		for (size_t i = minTickets - 1; i < ticketPerm.size(); ++i)
		{
			ticketDef.push_back(_tickets[ticketPerm[i]]);

			PathMetrics metric = findPathForTickets(ticketDef);
			metric.addPoints(cardPoints += ticketDef.back().points);

			if (bestPpt < metric.ppt && metric.length <= trainTokenLimit)
			{
				bestPpt = metric.ppt;
				bestTicket.clear();
				for (size_t j = 0; j < i + 1; ++j)
					bestTicket.push_back(&_tickets[ticketPerm[j]]);
			}
		}
	} while (next_permutation(ticketPerm.begin(), ticketPerm.end()));

	for (TicketDecision& t : _tickets)
		t.keep = false;
	for (TicketDecision* t : bestTicket)
	{
		t->keep = true;
		m_Tickets.push_back(*t);
	}

	cout << "keep " << bestTicket.size() << " tickets (" << bestPpt << "ppt)" << endl;
}

void PathMetrics::calcRoutes(const vector<Route*>& _routes)
{
	*this = PathMetrics();

	for (const Route* r : _routes)
	{
		trains += r->def->length;
		length += r->def->length;
		points += r->points();
		hops++;

		if (r->def->tunnle)
			trains++;
	}
	turns = hops + (trains + 1) / 2;
	addPoints(0);
}

void PathMetrics::addPoints(size_t _points)
{
	points += _points;
	ppt = (float)points / turns;
}
